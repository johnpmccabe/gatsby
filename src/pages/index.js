import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout >
    <p>Welcome to my new Gatsby site hosted in GitLab Pages.</p>
    <a href="https://twitter.com/johnpmccabe?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @johnpmccabe</a>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    <p>Stay tuned...</p>
  </Layout>
)

export default IndexPage
